#!/bin/bash

# Generate a list of random passwords

# Random Number PW
#NUM_PASSWORD="${RANDOM}"
#echo "${NUM_PASSWORD}"

# Three random numbers together.
THREE="${RANDOM}${RANDOM}${RANDOM}"
#echo "${THREE}"

# Use current date/time as base for password - Epoc and Nano
PASSWORD=$(date +%s%N)
#echo "${PASSWORD}"

# Password Hashs
HASHPW=$(date +%s%N${THREE}${THREE}${THREE}${PASSWORD} | sha256sum | head -c64)
#echo ${HASHPW}

# Add random special character
SPECIAL_CHAR=$(echo '!@#$%^&*()_-+=' | fold -w1 | shuf | head -c1)
echo "${HASHPW}${SPECIAL_CHAR}"
