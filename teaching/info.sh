#!/bin/bash

# Display information about command

# Display what the user typed
echo "You executed this command: ${0}"

# Colors
GREEN='\033[0;32m'
NC='\033[0m'

# Display the path and filename of the script.
#echo -e "You used ${GREEN}$(dirname ${0})${NC} as the path to the ${GREEN}$(basename ${0}${NC} script)"

printf "You used ${GREEN}$(dirname ${0})${NC} as the path to the ${GREEN}$(basename ${0}${NC} script)\n"

# Tell user how many arguments they passed in.
PARAMS="${#}"
echo "You supplied ${PARAMS} argument(s) on the command line."

# Supply at least one argument check
if [[ "${PARAMS}" -lt 1 ]]
then
    echo "Usage: ${0} USER_NAME [USER_NAME]..."
    exit 1
fi

# Generate and display a password for each parameter.
for USER_NAME in "${@}"
do
    PASSWORD=$(date +%s%N | sha256sum | head -c48)
    echo "${USER_NAME}: ${PASSWORD}"
done
